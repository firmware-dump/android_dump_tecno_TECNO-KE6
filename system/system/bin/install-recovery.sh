#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:40894464:324d69c56f87e4d56351fb498a208831b3629fa9; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:7f90e345f2909a7d86b82f3239c5b6d2e924e3a9 \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:40894464:324d69c56f87e4d56351fb498a208831b3629fa9 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
